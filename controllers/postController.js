const { post } = require('../models')

module.exports = {
    create: async (req,res) => {
        try {
            let data = await post.create({
                title: req.body.title,
                body: req.body.body
            })
            res.status(201).json({
                status: 'success',
                data: {data}
            })
        } catch (error) {
            res.status(404).json({
                status: 'fail',
                message: error.message
            })
        }
    },
    index: async (req, res) => {
        try {
            let data = await post.findAll()
            res.status(200).json({
                status: 'success',
                data: {data}
            })
        } catch (error) {
            res.status(404).json({
                status: 'fail',
                data: error.message
            })
        }
    }
}