const router = require('express').Router()

const index = require('./controllers/appController')
const post = require('./controllers/postController')

router.get('/', index.index)

router.post('/posts', post.create)
router.get('/posts', post.index)

module.exports = router