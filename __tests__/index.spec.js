const app = require('../app')
const request = require('supertest')

describe('root endpoint', () => {
    describe('GET /', () => {
        test('Should return 200', done => {
            request(app).get('/')
                .then(res => {
                    expect(res.body.status).toEqual('success')
                    expect(res.body.message).toEqual('Hello World')
                    done()
                })
        })
    })
})