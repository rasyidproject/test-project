const app = require('../app')
const request = require('supertest')
const { post } = require('../models')

describe('POST API Collection', () => {

    beforeAll( () => {
        return post.destroy({
            truncate: true
        })
    })

    afterAll( () => {
        return post.destroy({
            truncate: true
        })
    })

    describe('POST /posts', () => {
        test('Should succesfully create a new post', done => {
            request(app).post('/posts')
                .set('Content-Type','application/json')
                .send({title: 'Hello Mars', body: 'Im from Earth, nice to meet you'})
                .then(res => {
                    expect(res.statusCode).toEqual(201)
                    expect(res.body.status).toEqual('success')
                    expect(res.body.data).toHaveProperty('data')
                    done()
                })
        })
    })

    describe('GET /posts', () => {
        test('Should succesfully display all post', done => {
            request(app).get('/posts')
                .set('Content-Type','application/json')
                .then(res => {
                    expect(res.statusCode).toEqual(200)
                    expect(res.body.status).toEqual('success')
                    expect(res.body.data).toHaveProperty('data')
                    done()
                })
        })
    })
})